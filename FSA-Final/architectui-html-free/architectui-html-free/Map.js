//dropdown data
var dropdown_options = [
  {value: "current",
  text: "Current amount of loans in repayment"
},
  {value: "second",
  text: "Loans that are 31-90 days delinquent"
},
  {value: "third",
  text: "Loans that are 91-180 days delinquent"
},
  {value: "forth",
  text: "Loans that are 181-270 days delinquent"
},
  {value: "fifth",
  text: "Loans that are 271-360 days delinquent"
},
  {value: "last",
  text: "Loans that are 361 days or more delinquent"
},
]

//populate drop-down
d3.select("#dropdown")
  .selectAll("option")
  .data(dropdown_options)
  .enter()
  .append("option")
  .attr("value", function(option) { return option.value })
  .text(function(option) { return option.text });

//setup
var choro_chart_width = window.innerWidth;
var choro_chart_height = window.innerHeight;
var choro_color = d3.scaleQuantize().range([
'rgb(240,249,232)',
'rgb(204,235,197)',
'rgb(168,221,181)','rgb(123,204,196)',
'rgb(78,179,211)','rgb(43,140,190)',
'rgb(8,104,172)','rgb(8,64,129)']);

var formatDecimalComma = d3.format(",.2f");

var choro_div = d3.select('body')
  .append('div')
  .attr('class', 'tooltip')
  .style('opacity', 0);

//projection and path
var choro_projection = d3.geoAlbersUsa()
  .scale([choro_chart_width / 1.1])
  .translate([choro_chart_width / 22 , choro_chart_height / 25]);

var choro_path = d3.geoPath(choro_projection);

//create svg
var choro_svg = d3.select('#choro_chart')
  .append('svg')
  .attr('width', '100vw')
  .attr('height', '42vw')
  .attr('viewBox', `${-choro_chart_width / 3.5} ${-choro_chart_height / 2}
  ${choro_chart_width} ${choro_chart_height}`);

//loads the data to state variable
var state_data = d3.csv('StatesbyDollars.csv');
  state_data.then(function(state_data){
    state_data.forEach(function(d){
      d.current = +d.current;
      d.second = +d.second;
      d.third = +d.third;
      d.fourth = +d.fourth;
      d.fifth = +d.fifth;
      d.last = +d.last;
    })
});

// selects min and max from data set and scale colors based off that
state_data.then(function(state_data){
  choro_color.domain([
    d3.min(state_data, function(d ){
        return d.current;
    }),
    d3.max(state_data, function(d){
      return d.current;
    })
  ]);

//map data
d3.json('us.json').then(function(us_data){
  us_data.features.forEach(function(us_e, us_i){
    state_data.forEach(function(s_e, s_i){
      if(us_e.properties.name !== s_e.Name){
        return null;
      }
      us_data.features[us_i].properties.current = (s_e.current);
    });
  });

//create and fill map
  choro_svg.selectAll('path')
  .data(us_data.features)
  .enter()
  .append('path')
  .attr('d', choro_path)
  .attr('fill', function(d){
    var choro_num = d.properties.current;
    return choro_num ? choro_color(choro_num) : '#ddd';
  })
  .attr('stroke', '#000')
  .attr('stroke-width', 1)

// hoverover fill -- not currently working, conflicting with the tooltip
  .on('mouseover', function(){
    d3.select(this)
      .attr('fill', '#014636')
  .on('mouseout', function(){
    d3.select(this)
      .transition()
      .duration(750)
      .attr('fill', function(d){
        var choro_num = d.properties.current;
        return choro_num ? choro_color(choro_num) : '#ddd';
      })
      })
      })

      //tooltip
        .on('mouseover', d =>{
          choro_div
              .transition()
              .duration(200)
              .style('opacity', .9);
          choro_div
              .html(d.properties.name + '<br/>' + "$" +
              formatDecimalComma(d.properties.current))
              .style('left', d3.event.pageX + 'px')
              .style('top', d3.event.pageY - 28 + 'px');
        })
        .on ('mouseout', () =>{
          choro_div.transition()
              .duration(500)
              .style('opacity', 0);
        });
//Create buttons
// d3.selectAll('#buttons button')
//   .on('click', function(){
//     var choro_offset = choro_projection.translate();
//     var choro_distance = 50;
//     var choro_direction = d3.select(this).attr('class');

//     if (choro_direction == 'up'){
//       choro_offset[1] += choro_distance;
//     }else if (choro_direction == 'down') {
//       choro_offset[1] -= choro_distance;
//     }else if (choro_direction == 'left') {
//       choro_offset[0] += choro_distance;
//     }else if (choro_direction == 'right') {
//       choro_offset[0] -= choro_distance;
//     }

//     choro_projection.translate( choro_offset );

//     choro_svg.selectAll('path')
//       .transition()
//       .attr('d', choro_path);

//   });



// create the legend
var choro_legend = choro_svg.selectAll('g.legendEntry')
    .data(choro_color.range().reverse())
    .enter()
    .append('g').attr('class', 'legendEntry');

choro_legend
    .append('rect')
    .attr("x", choro_chart_width - 2050)
    .attr("y", function(d, i) {
       return i * 20;
    })
   .attr("width", 10)
   .attr("height", 10)
   .style("stroke", "black")
   .style("stroke-width", 1)
   .style("fill", function(d){return d;});
       //the data objects are the fill colors

choro_legend
    .append('text')
    .attr("x", choro_chart_width - 2035)
    .attr("y", function(d, i) {
       return i * 20;
    })
    .attr("dy", "0.8em") //place text one line *below* the x,y point
    .text(function(d,i) {
        var extent = choro_color.invertExtent(d);
        var format = d3.format(".3s");
        return format(+extent[0]) + " - " + format(+extent[1]);
    });
});
//Zoom the Map
// var zoom_map = d3.drag().on('drag', function(){
//   var choro_ofset1 = projection.translate();
//   choro_ofset1[0] += d3.event.dx;
//   choro_ofset1[1] += d3.event.dy;

//   choro_projection.translate(choro_ofset1);

//   choro_svg.selectAll('path')
//     .transition()
//     .attr('d', path);
// });

// var map = choro_svg.append('g')
//     .attr('')
// d3.selectAll('#buttons button.zooming').on('click', function(){
//       var choro_offset = choro_projection.translate();
//   var choro_scale = 1;
//   var choro_direction1 = d3.select(this).attr('class').replace('zooming', '');

//   if(choro_direction1 === 'in'){
//     choro_scale = 1.25;
//   }else if (choro_direction1 === 'out') {
//     choro_scale = 0.75;
//   }

//   choro_projection.translate( choro_offset );

//   choro_svg.selectAll('path')
//     .transition()
//     .attr('d', choro_path);
// });
});
