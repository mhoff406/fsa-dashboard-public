// create the svg
var  dnmargin = {top: 45, right: 75, bottom: 70, left: 100};
var  dnwidth = 1000 - dnmargin.left - dnmargin.right;
var  dnheight = 550 - dnmargin.top - dnmargin.bottom;

var tooltip = d3.select("#Delinquency").append("div")
// .attr("class", "toolTip");

// append the svg object to the body of the page
var dnsvg = d3.select("#Delinquency")
  .append("svg")
    .attr("width", dnwidth + dnmargin.left + dnmargin.right)
    .attr("height", dnheight + dnmargin.top + dnmargin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + dnmargin.left + "," + dnmargin.top + ")");
          

// Parse the Data
d3.csv("Delin.csv", function(data) {
    // Add X axis
    var dnmaxX = d3.max(data, function(d){
        return d.Value;
      });

    var x = d3.scaleLinear()
        .domain([0, dnmaxX])
        .range([ 0, dnwidth]);

    // var xAxisTick = number => 
    //       format('.3s')(number);
     
    // var xAxis = axisBottom(x)
    //     .tickFormat(xAxisTick);


    dnsvg.append("g")
        .attr("transform", "translate(0," + dnheight + ")")
        .call(d3.axisBottom(x))
        .selectAll("text")
        .attr("transform", "translate(-10,0)rotate(-45)")
        .style("text-anchor", "end");


    // Y axis
    var y = d3.scaleBand()
        .range([ 0, dnheight ])
        .domain(data.map(function(d) { return d.Age; }))
        .padding(.1);

    dnsvg.append("g")
        .call(d3.axisLeft(y))

    //Bars
    dnsvg.selectAll("myRect")
        .data(data)
        .enter()
        .append("rect")
        .attr("x", x(0) )
        .attr("y", function(d) { return y(d.Age); })
        .attr("width", function(d) { return x(d.Value); })
        .attr("height", y.bandwidth() )
        .attr("fill", "#69b3a2")
        .on('mouseover', d => {
            tooltip
              .transition()
              .duration(200)
              .style('opacity', 1)
            tooltip
              .html((d3.format("s")(d.Value)))
              .style('left', d3.event.pageX + 'px')
              .style('top', d3.event.pageY - 1100 + 'px')
              .style("display", "inline-block");
         })
        .on('mouseout', ()=> {
            tooltip.style("display", "none");
            //   .transition()
            //   .duration(500)
            //   .style('opacity', 0);
          });

});