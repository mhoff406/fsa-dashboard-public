// create the svg

  var  b_smargin = {top: 50, right: 50, bottom: 50, left: 50};
  var  b_swidth = 775 - b_smargin.left - b_smargin.right;
  var  b_sheight = 550 - b_smargin.top - b_smargin.bottom;

// set x scale
var b_sx = d3.scaleBand()
    .rangeRound([0, b_swidth])
    .paddingInner(0.05)
    .align(0.1);

// set y scale
var b_sy_scale = d3.scaleLinear()
    .range([b_sheight, 0]);

    
    
// set the colors
var b_sz = d3.scaleOrdinal()
    .range(["#f7fcf0", "#e0f3db", "#ccebc5", "#a8ddb5", "#7bccc4", "#4eb3d3", "#74a9cf", "#084081"]);

// load the csv and create the chart
var b_sdata = d3.csv("Port_Loan_Status.csv");
b_sdata.then(function(b_sdata){
  b_sdata.forEach(function(d){
  d.Default = +d.Default;
  d.Default1= +d.Default1;
  d.Deferment = +d.Deferment;
  d.Forbearance = +d.Forbearance;
  d.Grace = +d.Grace;
  d.School = +d.School;
  d.Other = +d.Other;
  d.Repayment = +d.Repayment
})

//Define the div for the tooltip
var b_sdiv = d3.select('body')
    .append('div')
    .attr('class', 'tooltip')
    .style('opacity', 0);

/// Create SVG Element
var b_ssvg  =   d3.select( '#B_SChart' )
      .append( 'svg' )
      .attr( 'width',b_swidth + b_smargin.left + b_smargin.right)
      .attr( 'height',b_sheight + b_smargin.top + b_smargin.bottom);

var b_sgroups = b_ssvg.append("g")
    .attr("transform", "translate(" + 100 + "," + b_smargin.top + ")");

var b_skeys = b_sdata.columns.slice(1);

  b_sdata.sort(function(a, b) { return b.total - a.total; });

  var b_smaxY = d3.max(b_sdata, function(d){
    return d.Default+
           d.Default1 +
           d.Deferment +
           d.Forbearance +
           d.Grace +
           d.School +
           d.Other +
           d.Repayment ;
  });

  b_sx.domain(b_sdata.map(function(d) { return d.Year; }));
  b_sy_scale.domain([0, b_smaxY]);
  b_sz.domain(b_skeys);

  b_sgroups.append("g")
      .selectAll("g")
      .data(d3.stack().keys(b_skeys)(b_sdata))
      .enter().append("g")
      .attr("fill", function(d) { return b_sz(d.key); })
      .selectAll("rect")
      .data(function(d) { return d; })
      .enter().append("rect")
      .attr("x", function(d) { return b_sx(d.data.Year); })
      .attr("y", function(d) { return b_sy_scale(d[1]); })
      .attr("height", function(d) {
        return b_sy_scale(d[0]) - b_sy_scale(d[1]); })
      .attr("width", b_sx.bandwidth())
      .on('mouseover', d => {
        b_sdiv
          .transition()
          .duration(200)
          .style('opacity', 0.9)
        b_sdiv
          .html(d.data.Year + '<br/>' + (d[1]-d[0]))
          .style('left', d3.event.pageX + 'px')
          .style('top', d3.event.pageY - 28 + 'px');
      })
      .on('mouseout', ()=> {
        b_sdiv
          .transition()
          .duration(500)
          .style('opacity', 0);
      });

  b_sgroups.append("g")
      .attr("class", "axis")
      .attr("transform", "translate(0," + b_sheight + ")")
      .call(d3.axisBottom(b_sx))
      .selectAll("text")
      .style("text-anchor", 'end')
      .attr('dx', '-.8em')
      .attr('dy', '.15em')
      .attr("transform", 'rotate(-55)');


  b_sgroups.append("g")
      .attr("class", "axis")
      .call(d3.axisLeft(b_sy_scale).ticks(null, "s"))
      .append("text")
      .attr("x", 2)
      .attr("y", b_sy_scale(b_sy_scale.ticks().pop()) + 0.5)
      .attr("dy", "0.32em")
      .attr("fill", "#000")
      .attr("font-weight", "bold")
      .attr("text-anchor", "start");


  var b_slegend = b_sgroups.append("g")
      .attr("font-family", "sans-serif")
      .attr("font-size", 10)
      .attr("text-anchor", "front")
      .selectAll("g")
      .data(b_skeys.slice())
      .enter().append("g")
      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

  b_slegend.append("rect")
      .attr("x", b_swidth + 10)
      .attr("width", 19)
      .attr("height", 19)
      .attr("fill", b_sz);

  b_slegend.append("text")
      .attr("x", b_swidth + 35)
      .attr("y", 9.5)
      .attr("dy", "0.32em")
      .text(function(d) { return d; });

  var keys1 = ["Default", "Default1", "Deferment", "Forbearance"] 
  var keys2 = ["Grace", "School", "Other", "Repayment"]

      // Usually you have a color1 scale in your chart already
      var color1 = d3.scaleOrdinal()
        .domain(keys1)
        .range(["#f7fcf0", "#e0f3db", "#ccebc5", "#a8ddb5"]);
           
      // Add one dot in the legend for each name.
      var size = 20
      b_ssvg.selectAll("mydots")
        .data(keys1)
        .enter()
        .append("rect")
          .attr("x", 120)
          .attr("y", function(d,i){ return 50 + i*(size+5)}) // 100 is where the first dot appears. 25 is the distance between dots
          .attr("width", size)
          .attr("height", size)
          .style("fill", function(d){ return color1(d)})
      
      // Add one dot in the legend for each name.
      b_ssvg.selectAll("mylabels")
        .data(keys1)
        .enter()
        .append("text")
          .attr("x", 120 + size*1.2)
          .attr("y", function(d,i){ return 50 + i*(size+5) + (size/2)}) // 100 is where the first dot appears. 25 is the distance between dots
          .text(function(d){ return d})
          .attr("text-anchor", "left")
          .style("alignment-baseline", "middle")

        var color2 = d3.scaleOrdinal()
        .domain(keys1)
        .range(["#7bccc4", "#4eb3d3", "#74a9cf", "#084081"]);
      
      // Add one dot in the legend for each name.
      var size = 20
      b_ssvg.selectAll("mydots")
        .data(keys2)
        .enter()
        .append("rect")
          .attr("x", 240)
          .attr("y", function(d,i){ return 50 + i*(size+5)}) // 100 is where the first dot appears. 25 is the distance between dots
          .attr("width", size)
          .attr("height", size)
          .style("fill", function(d){ return color2(d)})
      
      // Add one dot in the legend for each name.
      b_ssvg.selectAll("mylabels")
        .data(keys2)
        .enter()
        .append("text")
          .attr("x", 240 + size*1.2)
          .attr("y", function(d,i){ return 50 + i*(size+5) + (size/2)}) // 100 is where the first dot appears. 25 is the distance between dots
          .text(function(d){ return d})
          .attr("text-anchor", "left")
          .style("alignment-baseline", "middle")

   });
