///create margin variables

    var sun_width = 900;
    var sun_height = 600;
    var sun_maxRadius = (Math.min(sun_width, sun_height) / 2.5)


///grab data
var sun_data = {
    "name": "Portfolio Summary",
    "children": [
    {
        "name": "Direct Loan",
        "children": [
        {"name": "In-School", "value": 130.1},
        {"name": "Grace", "value": 24.7},
        {
          "name": "Repayment",
          "children":[
          {"name": "Level: \n 10 Yrs or Less", "value": 219.9},
          {"name": "Level: Greater than 10 Yrs", "value": 78.2},
          {"name": "Graduated: 10 Yrs or less", "value": 88.3},
          {"name": "Graduated: 10 Yrs or Greater", "value": 15.8},
          {"name": "Income-Contingent", "value": 30.9},
          {
            "name": "Income Based",
            "children":[
              {"name": "Partial Financial Hardship", "value": 123.5},
              {"name": "No Partial Financial Hardship", "value": 46.3},
            ]
          },
          {
              "name": "Pay As You Earn",
              "children":[
                  {"name": "Partial Financial Hardship", "value": 72.1},
                  {"name": "No Partial Financial Hardship", "value": 13.2},
              ]
          },
          {"name": "Repaye", "value": 144.8},
          {"name": "Alternative", "value": 36.6},
          {"name": "Other", "value": 24.8},
          ]
        },
        {
          "name": "Deferment",
          "children":[
              {"name": "In-School", "value": 93.8},
              {"name": "Six-Month Post-Enrollment", "value": 6.1},
              {"name": "Unemployment", "value": 6.2},
              {"name": "Economic Hardship", "value": 3.5},
              {"name": "Military", "value": 0.2},
              {"name": "Cancer Treatment", "value":0},
              {"name": "Other", "value": 0.6},
              {"name": "Not Reported", "value": 0.0},
          ]
      },
      {
          "name": "Forbearance",
          "children":[
              {"name": "Administrative", "value": 38.2},
              {"name": "Discretionary", "value": 71.3},
              {"name": "Mandatory Administrative", "value": 2.1},
              {"name": "Mandatory", "value": 7.1},
              {"name": "Not Reported", "value": 0.0},
          ]
      },
        {"name": "Cumulative in Default", "value": 105.8},
        {"name": "Other", "value": 8.4}
        ]
      },
        {
        "name": "FFEL",
        "children": [
            {"name": "In-School", "value": 0.4},
            {"name": "Grace", "value": 0.1},
        {
          "name": "Repayment",
          "children":[
          {"name": "Level: 10 Yrs or Less", "value": 17.05},
          {"name": "Level: 10 Yrs or Greater", "value": 2.04},
          {"name": "Graduated: 10 Yrs or less", "value": 5.61},
          {"name": "Graduated: 10 Yrs or Greater", "value": 0.01},
          {
            "name": "Income Based",
            "children":[
              {"name": "Partial Financial Hardship", "value": 15.99},
              {"name": "No Partial Financial Hardship", "value": 7.64},
            ]
          },
          {"name": "Alternative", "value": 0.14},
          {"name": "Other", "value": 0.37}
          ]
        },
        {
          "name": "Deferment",
          "children":[
              {"name": "In-School", "value": 9.7},
              {"name": "Six-Month Post-Enrollment", "value": 0.1},
              {"name": "Unemployment", "value": 1.5},
              {"name": "Economic Hardship", "value": 1.0},
              {"name": "Military", "value": 0.1},
              {"name": "Cancer Treatment", "value":0},
              {"name": "Other", "value": 0.1},
              {"name": "Not Reported", "value": 0.0},
          ]
      },
        {"name": "Forbearance", "value": 23.5},
        {"name": "Cumulative in Default", "value":66.5},
        {"name": "Other", "value": 3.6}
        ]
    },
        {
        "name": "Perkins", "value": 6.9
  }
]
}

      var formatNumber = d3.format(',d');

      var sun_x = d3.scaleLinear()
          .range([0, 2 * Math.PI])
          .clamp(true);

      var sun_y = d3.scaleSqrt()
          .range([sun_maxRadius*.1, sun_maxRadius]);

    var sun_color = d3.scaleOrdinal(["#f7fcf0", "#ccebc5", "#a8ddb5", "#7bccc4", "#4eb3d3", "#74a9cf", "#084081"]);

      var sun_partition = d3.partition();
///create the arcs
      var sun_arc = d3.arc()
          .startAngle(d => sun_x(d.x0))
          .endAngle(d => sun_x(d.x1))
          .innerRadius(d => Math.max(0, sun_y(d.y0)))
          .outerRadius(d => Math.max(0, sun_y(d.y1)));

      var sun_middleArcLine = d => {
          var sun_halfPi = Math.PI/2;
          var sun_angles = [sun_x(d.x0) - sun_halfPi, sun_x(d.x1) - sun_halfPi];
          var sun_r = Math.max(0, (sun_y(d.y0) + sun_y(d.y1)) / 2);

          var sun_middleAngle = (sun_angles[1] + sun_angles[0]) / 2;
          var sun_invertDirection = sun_middleAngle > 0 && sun_middleAngle < Math.PI; // On lower quadrants write text ccw
          if (sun_invertDirection) { sun_angles.reverse(); }

          var sun_path = d3.path();
          sun_path.arc(0, 0, sun_r, sun_angles[0], sun_angles[1], sun_invertDirection);
          return sun_path.toString();
      };
///create labels, and orient them
      var sun_textFits = d => {
          var CHAR_SPACE = 6;

          var sun_deltaAngle = sun_x(d.x1) - sun_x(d.x0);
          var sun_r = Math.max(0, (sun_y(d.y0) + sun_y(d.y1)) / 2);
          var sun_perimeter = sun_r * sun_deltaAngle;

          return d.data.name.length * CHAR_SPACE < sun_perimeter;
      };
///create svg element
      var sun_svg = d3.select('#Sunburst').append('svg')
          .style('width', '70vw')
          .style('height', '80vh')
          .attr('viewBox', `${-sun_width / 3} ${-sun_height / 2.5} ${sun_width} ${sun_height}`)
          .on('click', () => focusOn()); // Reset zoom on canvas click


          // Find the root node of our data, and begin sizing process.
          var sun_root = d3.hierarchy(sun_data)
              .sum(function (d) { return d.value});

          var sun_slice = sun_svg.selectAll('g.slice')
              .data(sun_partition(sun_root).descendants());

          sun_slice.exit().remove();

          var sun_newSlice = sun_slice.enter()
              .append('g').attr('class', 'slice')
              .on('click', d => {
                  d3.event.stopPropagation();
                  focusOn(d);
              });

          sun_newSlice.append('title')
              .text(d => d.data.name + '\n' + formatNumber(d.value));

          sun_newSlice.append('path')
              .attr('class', 'main-arc')
              .style('fill', d => sun_color((d.children ? d : d.parent).data.name))
              .attr('d', sun_arc);

          sun_newSlice.append('path')
              .attr('class', 'hidden-arc')
              .attr('id', (_, i) => `hiddenArc${i}`)
              .attr('d', sun_middleArcLine);

          var sun_text = sun_newSlice.append('text')
              .attr('display', d => sun_textFits(d) ? null : 'none');

          // Add white contour
          sun_text.append('textPath')
              .attr('startOffset','50%')
              .attr('xlink:href', (_, i) => `#hiddenArc${i}` )
              .text(d => d.data.name)
              .style('fill', 'none')
              .style('stroke', '#fff')
              .style('stroke-width', 0)
              .style('stroke-linejoin', 'round');

          sun_text.append('textPath')
              .attr('startOffset','50%')
              .attr('xlink:href', (_, i) => `#hiddenArc${i}` )
              .text(d => d.data.name);


      function focusOn(d = { x0: 0, x1: 1, y0: 0, y1: 1 }) {
          // Reset to top-level if no data point specified


///create Transition
          var sun_transition = sun_svg.transition()
              .duration(750)
              .tween('scale', () => {
                  var sun_xd = d3.interpolate(sun_x.domain(), [d.x0, d.x1]),
                      sun_yd = d3.interpolate(sun_y.domain(), [d.y0, 1]);
                  return t => { sun_x.domain(sun_xd(t)); sun_y.domain(sun_yd(t)); };
              });

          sun_transition.selectAll('path.main-arc')
              .attrTween('d', d => () => sun_arc(d));

          sun_transition.selectAll('path.hidden-arc')
              .attrTween('d', d => () => sun_middleArcLine(d));

          sun_transition.selectAll('text')
              .attrTween('display', d => () => sun_textFits(d) ? null : 'none');

          moveStackToFront(d);

          //

          function moveStackToFront(elD) {
              sun_svg.selectAll('.slice').filter(d => d === elD)
                  .each(function(d) {
                      this.parentNode.appendChild(this);
                      if (d.parent) { moveStackToFront(d.parent); }
                  })
}}
