var margin = { top: 0, right: 50, bottom: 50, left: 50 },
            width = 775 - margin.left - margin.right,
            height = 550 - margin.top - margin.bottom;

        var x0 = d3.scaleBand().range([0, width]).round(true);

        var x1 = d3.scaleBand();

        var y1 = d3.scaleLinear()
            .range([height, 0]);

        var color = d3.scaleOrdinal()
            .range(["#2DBAD4", "#33CC33", "#00E6B8", "#C9081D"]);

        var xAxis = d3.axisBottom(x0);

        var yAxis = d3.axisLeft(y1)
            .tickFormat(d3.format(".2s"));

        var svg = d3.select("#pcaChart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        d3.csv("PCArecovery.csv").then(data => {
          
            var categories = d3.keys(data[0]).filter(function (key) {
                
                return key !== "Year";
            });

            data.forEach(function (d) {
                //first calculate the starting height for each category
                total = 0
                totals = {}
                for (var i = 0; i < categories.length; i++) {
                    if (i != 0 && i != 1) {
                        total = total - +d[categories[i]];
                        totals[categories[i]] = total;
                        
                    }
                    else {
                        totals[categories[i]] = total;
                        total += +d[categories[i]];
                      
                    }
                }
                //then map category name, value, and starting height to each observation
                d.formatted = categories.map(function (category) {
                    return {
                        name: category,
                        value: +d[category],
                        baseHeight: +totals[category]
                    };
                });
            });

            x0.domain(data.map(function (d) { return d.Year; }));
            x1.domain(categories).range([0, x0.bandwidth()]);
            
            y1.domain([0, d3.max(data, function (d) {
                return d3.max(d.formatted, function (d) {
                    return d.value + d.baseHeight;
                });
            })]);

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            svg.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(-5, 0)")
                .call(yAxis)

            var state = svg.selectAll(".state")
                .data(data)
                .enter().append("g")
                .attr("class", "g")
                .attr("transform", function (d) {
                    return "translate(" + x0(d.Year) + ",0)";
                });

            state.selectAll("rect")
                .data(function (d) { return d.formatted; })
                .enter().append("rect")
                .attr("width", x1.bandwidth())
                .attr("x", function (d) { return x1(d.name); })
                .attr("y", function (d) { return y1(d.value + d.baseHeight); })
                .attr("height", function (d) { return height - y1(d.value); })
                .style("fill", function (d) { return color(d.name); });

            var legend = svg.selectAll(".legend")
                .data(categories.slice().reverse())
                .enter().append("g")
                .attr("class", "legend")
                .attr("transform", function (d, i) {
                    return "translate(0," + i * 20 + ")";
                });

            legend.append("rect")
                .attr("x", width - 665)
                .attr("width", 18)
                .attr("height", 18)
                .style("fill", color);

            legend.append("text")
                .attr("x", width - 640)
                .attr("y", 9)
                .attr("dy", ".35em")
                .style("text-anchor", "beginning")
                .text(function (d) { return d; });
        });