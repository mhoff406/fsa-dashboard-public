// create the svg
var  dlmargin = {top: 50, right: 50, bottom: 50, left: 50};
var  dlwidth = 1150 - dlmargin.left - dlmargin.right;
var  dlheight = 550 - dlmargin.top - dlmargin.bottom;

// set x scale
var dlx = d3.scaleBand()
    .rangeRound([0, dlwidth])
    .paddingInner(0.05)
    .align(0.1);

// set y scale
var dly = d3.scaleLinear()
    .range([dlheight, 0]);

// set the colors
var dlz = d3.scaleOrdinal()
    .range(["#7bccc4", "#e0f3db"]);

// load the csv and create the chart
var dldata = d3.csv("DirectLoanStatus.csv");

dldata.then(function(dldata){
  dldata.forEach(function(d){
  d.Deferment= +d.Deferment;
  d.Forbearance= +d.Forbearance
})

//Define the div for the tooltip
var div = d3.select('body')
    .append('div')
    .attr('class', 'tooltip')
    .style('opacity', 0);

/// Create SVG Element
var dlsvg  =   d3.select( '#DLstatus' )
      .append( 'svg' )
      .attr( 'width',dlwidth + dlmargin.left + dlmargin.right)
      .attr( 'height',dlheight + dlmargin.top + dlmargin.bottom);
   
var dlgroups = dlsvg.append("g")
    .attr("transform", "translate(" + 100 + "," + dlmargin.top + ")");

var dlkeys = dldata.columns.slice(1);

dldata.sort(function(a, b) { return b.total - a.total; });

var dlmaxY = d3.max(dldata, function(d){
  return  d.Deferment +
          d.Forbearance;
});

var formatDecimal = d3.format(".1f");
  

dlx.domain(dldata.map(function(d) { return d.Year; }));
dly.domain([0, dlmaxY]);
dlz.domain(dlkeys);

dlgroups.append("g")
    .selectAll("g")
    .data(d3.stack().keys(dlkeys)(dldata))
    .enter().append("g")
    .attr("fill", function(d) {  return dlz(d.key); })
    .selectAll("rect")
    .data(function(d) { return d; })
    .enter().append("rect")
    .attr("x", function(d) { return dlx(d.data.Year); })
    .attr("y", function(d) { return dly(d[1]); })
    .attr("height", function(d) {
      return dly(d[0]) - dly(d[1]); })
    .attr("width", dlx.bandwidth())
    .on('mouseover', d => {
      div
        .transition()
        .duration(200)
        .style('opacity', 1)
      div
        .html(d.data.Year + '<br/>' + (d3.format(".1f")(d[1]-d[0])))
        .style('left', d3.event.pageX + 'px')
        .style('top', d3.event.pageY - 28 + 'px');
    })
    .on('mouseout', ()=> {
      div
        .transition()
        .duration(500)
        .style('opacity', 0);
    });
    

dlgroups.append("g")
    .attr("class", "axis")
    .attr("transform", "translate(0," + dlheight + ")")
    .call(d3.axisBottom(dlx))
    .selectAll("text")
    .style("text-anchor", 'end')
    .attr('dx', '-.8em')
    .attr('dy', '.15em')
    .attr("transform", 'rotate(-45)');


dlgroups.append("g")
    .attr("class", "axis")
    .call(d3.axisLeft(dly).ticks(null, "s"))
    .append("text")
    .attr("x", 2)
    .attr("y", dly(dly.ticks().pop()) + 0.5)
    .attr("dy", "0.32em")
    .attr("fill", "#000")
    .attr("font-weight", "bold")
    .attr("text-anchor", "start");


var blegend = dlgroups.append("g")
    .attr("font-family", "sans-serif")
    .attr("font-size", 10)
    .attr("text-anchor", "front")
    .selectAll("g")
    .data(dlkeys.slice())
    .enter().append("g")
    .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

blegend.append("rect")
    .attr("x", dlwidth + 10)
    .attr("width", 19)
    .attr("height", 19)
    .attr("fill", dlz);

blegend.append("text")
    .attr("x", dlwidth + 35)
    .attr("y", 9.5)
    .attr("dy", "0.32em")
    .text(function(d) { return d; });

    var keys3 = ["Forbearance","Deferment"]
    // var keys4 = ["Forebearance", "Default", "Other"]

    // Usually you have a color scale in your chart already
    var color3 = d3.scaleOrdinal()
      .domain(keys3)
      .range(["#7bccc4", "#e0f3db"]);
    
    // Add one dot in the legend for each name.
    var size = 20
    dlsvg.selectAll("mydots")
      .data(keys3)
      .enter()
      .append("rect")
        .attr("x", 120)
        .attr("y", function(d,i){ return 50 + i*(size+5)})
        .attr("width", size)
        .attr("height", size)
        .style("fill", function(d){ return color3(d)})
    
    // Add one dot in the legend for each name.
    dlsvg.selectAll("mylabels")
      .data(keys3)
      .enter()
      .append("text")
        .attr("x", 120 + size*1.2)
        .attr("y", function(d,i){ return 50 + i*(size+5) + (size/2)}) 
        .text(function(d){ return d})
        .attr("text-anchor", "left")
        .style("alignment-baseline", "middle")

      //   var color4 = d3.scaleOrdinal()
      //   .domain(keys4)
      //   .range(["#4ac16d","#1fa187","#277f8e"]);
      
      // // Add one dot in the legend for each name.
      // var size = 20
      // dlsvg.selectAll("mydots")
      //   .data(keys4)
      //   .enter()
      //   .append("rect")
      //     .attr("x", 240)
      //     .attr("y", function(d,i){ return 50 + i*(size+5)}) // 100 is where the first dot appears. 25 is the distance between dots
      //     .attr("width", size)
      //     .attr("height", size)
      //     .style("fill", function(d){ return color4(d)})
      
      // // Add one dot in the legend for each name.
      // dlsvg.selectAll("mylabels")
      //   .data(keys4)
      //   .enter()
      //   .append("text")
      //     .attr("x", 240 + size*1.2)
      //     .attr("y", function(d,i){ return 50 + i*(size+5) + (size/2)}) // 100 is where the first dot appears. 25 is the distance between dots
      //     .text(function(d){ return d})
      //     .attr("text-anchor", "left")
      //     .style("alignment-baseline", "middle")


  });