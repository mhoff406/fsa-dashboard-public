// // set the dimensions and margins of the graph
var zzmargin = {top: 50, right: 50, bottom: 50, left: 100},
    zzwidth = 1150 - zzmargin.left - zzmargin.right,
    zzheight = 550 - zzmargin.top - zzmargin.bottom;

// // set the dimensions and margins of the graph
// var margin = {top: 20, right: 20, bottom: 30, left: 50},
//     width = 960 - margin.left - margin.right,
//     height = 500 - margin.top - margin.bottom;

// parse the date / time
var parseTime = d3.timeParse("%Y-%m-%d");

// set the ranges
var px = d3.scaleTime().range([0, zzwidth]);
var py = d3.scaleLinear().range([zzheight, 0]);

// define the 1st line
var valueline = d3.line()
    .x(function(d) { return px(d.Year); })
    .y(function(d) { return py(d.IDR); });

// define the 2nd line
var valueline2 = d3.line()
    .x(function(d) { return px(d.Year); })
    .y(function(d) { return py(d.Non_IDR); });

// append the svg obgect to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var Asvg = d3.select("#IDR").append("svg")
    .attr("width", zzwidth + zzmargin.left + zzmargin.right)
    .attr("height", zzheight + zzmargin.top + zzmargin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + zzmargin.left + "," + zzmargin.top + ")");

// Get the data
d3.csv("IDR and NON-IDR.csv").then(function(data) {

  // format the data
  data.forEach(function(d) {
      d.Year = parseTime(d.Year);
      d.IDR = +d.IDR;
      d.Non_IDR = +d.Non_IDR;
  });

  // Scale the range of the data
  px.domain(d3.extent(data, function(d) { return d.Year; }));
  py.domain([0, d3.max(data, function(d) {
	  return Math.max(d.IDR, d.Non_IDR); })]);

  // Add the valueline path.
  Asvg.append("path")
      .data([data])
      .attr("class", "line")
      .attr("fill", "none")
      .attr("stroke", "steelblue")
      .attr("d", valueline);

  // Add the valueline2 path.
  Asvg.append("path")
      .data([data])
      .attr("class", "line")
      .attr("fill", "none")
      .style("stroke", "red")
      .attr("d", valueline2);

  // Add the X Axis
  Asvg.append("g")
      .attr("transform", "translate(0," + zzheight + ")")
      .call(d3.axisBottom(px));

  // Add the Y Axis
  Asvg.append("g")
      .call(d3.axisLeft(py));

});