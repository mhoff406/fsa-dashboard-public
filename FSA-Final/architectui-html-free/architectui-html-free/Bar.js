// create the svg
var  bmargin = {top: 50, right: 50, bottom: 50, left: 50};
var  bwidth = 1150 - bmargin.left - bmargin.right;
var  bheight = 550 - bmargin.top - bmargin.bottom;

// set x scale
var bx = d3.scaleBand()
    .rangeRound([0, bwidth])
    .paddingInner(0.05)
    .align(0.1);

// set y scale
var by_scale = d3.scaleLinear()
    .range([bheight, 0]);

// set the colors
var bz = d3.scaleOrdinal()
    .range(["#e0f3db", "#7bccc4", "#084081"]);

// load the csv and create the chart
var bdata = d3.csv("PortfolioSummary.csv");
bdata.then(function(bdata){
  bdata.forEach(function(d){
  d.Direct = +d.Direct;
  d.FFEL= +d.FFEL;
  d.Perkins = +d.Perkins
})

//Define the div for the tooltip
var div = d3.select('body')
    .append('div')
    .attr('class', 'tooltip')
    .style('opacity', 0);

/// Create SVG Element
var bsvg  =   d3.select( '#BChart' )
      .append( 'svg' )
      .attr( 'width',bwidth + bmargin.left + bmargin.right)
      .attr( 'height',bheight + bmargin.top + bmargin.bottom);

      
var bgroups = bsvg.append("g")
    .attr("transform", "translate(" + 100 + "," + bmargin.top + ")");

var bkeys = bdata.columns.slice(1);

bdata.sort(function(a, b) { return b.total - a.total; });

var bmaxY = d3.max(bdata, function(d){
  return d.Direct+
         d.FFEL +
         d.Perkins ;
});


// var formatDecimal = d3.format(".1f");
  

bx.domain(bdata.map(function(d) { return d.Year; }));
by_scale.domain([0, bmaxY]);
bz.domain(bkeys);

bgroups.append("g")
    .selectAll("g")
    .data(d3.stack().keys(bkeys)(bdata))
    .enter().append("g")
    .attr("fill", function(d) {  return bz(d.key); })
    .selectAll("rect")
    .data(function(d) { return d; })
    .enter().append("rect")
    .attr("x", function(d) { return bx(d.data.Year); })
    .attr("y", function(d) { return by_scale(d[1]); })
    .attr("height", function(d) {
      return by_scale(d[0]) - by_scale(d[1]); })
    .attr("width", bx.bandwidth())
    .on('mouseover', d => {
      div
        .transition()
        .duration(200)
        .style('opacity', 1)
      div
        .html(d.data.Year + '<br/>' + (d3.format(".1f")(d[1]-d[0])))
        .style('left', d3.event.pageX + 'px')
        .style('top', d3.event.pageY - 28 + 'px');
    })
    .on('mouseout', ()=> {
      div
        .transition()
        .duration(500)
        .style('opacity', 0);
    });
    

bgroups.append("g")
    .attr("class", "axis")
    .attr("transform", "translate(0," + bheight + ")")
    .call(d3.axisBottom(bx))
    .selectAll("text")
    .style("text-anchor", 'end')
    .attr('dx', '-.8em')
    .attr('dy', '.15em')
    .attr("transform", 'rotate(-45)');


bgroups.append("g")
    .attr("class", "axis")
    .call(d3.axisLeft(by_scale).ticks(null, "s"))
    .append("text")
    .attr("x", 2)
    .attr("y", by_scale(by_scale.ticks().pop()) + 0.5)
    .attr("dy", "0.32em")
    .attr("fill", "#000")
    .attr("font-weight", "bold")
    .attr("text-anchor", "start");

  // bgroups.append("text")
  //   .attr("class", "Bar-title")
  //   .attr("y", -45)
  //   .text("Portfolio Summary By Loan Type (in Billions Dollars)")
  //   .attr("font-size", "2.5em");


var blegend = bgroups.append("g")
    .attr("font-family", "sans-serif")
    .attr("font-size", 10)
    .attr("text-anchor", "front")
    .selectAll("g")
    .data(bkeys.slice())
    .enter().append("g")
    .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

blegend.append("rect")
    .attr("x", bwidth + 10)
    .attr("width", 19)
    .attr("height", 19)
    .attr("fill", bz);

blegend.append("text")
    .attr("x", bwidth + 35)
    .attr("y", 9.5)
    .attr("dy", "0.32em")
    .text(function(d) { return d; });

var keys = ["Direct Loan (DL)", "Federal Family Education Loan (FFEL)", "Perkins Loan (PL)"]

// Usually you have a color scale in your chart already
var color = d3.scaleOrdinal()
  .domain(keys)
  .range(["#e0f3db", "#7bccc4", "#084081"]);

// Add one dot in the legend for each name.
var size = 20
bsvg.selectAll("mydots")
  .data(keys)
  .enter()
  .append("rect")
    .attr("x", 120)
    .attr("y", function(d,i){ return 50 + i*(size+5)})
    .attr("width", size)
    .attr("height", size)
    .style("fill", function(d){ return color(d)})

// Add one dot in the legend for each name.
bsvg.selectAll("mylabels")
  .data(keys)
  .enter()
  .append("text")
    .attr("x", 120 + size*1.2)
    .attr("y", function(d,i){ return 50 + i*(size+5) + (size/2)}) 
    .text(function(d){ return d})
    .attr("text-anchor", "left")
    .style("alignment-baseline", "middle")
    });