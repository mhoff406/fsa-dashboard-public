// set the dimensions and margins of the graph
var margin = {top: 50, right: 25, bottom: 50, left: 100},
    width = 775 - margin.left - margin.right,
    height = 350 - margin.top - margin.bottom;

// parse the Year / time
// var parseTime = d3.timeParse("%Y");

// set the ranges
var xl = d3.scaleBand()
.rangeRound([0, width])
.paddingInner(0.05)
.align(0.1);

var yl = d3.scaleLinear().range([height, 0]);

// define the line
var valueline0 = d3.line()
    .x(function(d) { return xl(d.Year); })
    .y(function(d) { return yl(d.rates); })
    .curve(d3.curveMonotoneX) ;


// append the svg obgect to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var line_svg = d3.select("#Line2").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

// Get the data
d3.csv("DollarsInRepayment.csv").then(function(data) {
console.log(data);
  // format the data
  data.forEach(function(d) {
      d.Year = d.Year;
      d.rates = +d.rates;
  });

  // Scale the range of the data
  xl.domain(data.map(function(d) { return d.Year; }));
  yl.domain([0, d3.max(data, function(d) { return d.rates; })]);

  // Add the valueline path.
  line_svg.append("path")
      .data([data])
      .attr("class", "line")
      .attr("d", valueline0);
  


  // Add the x Axis
  line_svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(xl))
      .selectAll("text")
    .style("text-anchor", 'end')
    .attr('dx', '-.8em')
    .attr('dy', '.15em')
    .attr("transform", 'rotate(-55)');

  // Add the y Axis
  line_svg.append("g")
      .call(d3.axisLeft(yl));

      // Add datapoints
  //     svg.selectAll(".dot")
  //   .data(data)
  // .enter().append("circle") // Uses the enter().append() method
  //   .attr("class", "dot") // Assign a class for styling
  //   .attr("cx", function(d) { return x(d.Year) })
  //   .attr("cy", function(d) { return y(d.rates) })
  //   .attr("r", 4);
    

      var keys = ["Default", "Default*"]

      // Usually you have a color scale in your chart already
      var color = d3.scaleOrdinal()
        .domain(keys)
        .range(["steelblue", "red"]);
      
      // Add one dot in the legend for each name.
      var size = 20
      svg.selectAll("mydots")
        .data(keys)
        .enter()
        .append("rect")
          .attr("x", 120)
          .attr("y", function(d,i){ return 50 + i*(size+5)}) // 100 is where the first dot appears. 25 is the distance between dots
          .attr("width", size)
          .attr("height", size)
          .style("fill", function(d){ return color(d)})
      
      // Add one dot in the legend for each name.
      svg.selectAll("mylabels")
        .data(keys)
        .enter()
        .append("text")
          .attr("x", 120 + size*1.2)
          .attr("y", function(d,i){ return 50 + i*(size+5) + (size/2)}) // 100 is where the first dot appears. 25 is the distance between dots
          .text(function(d){ return d})
          .attr("text-anchor", "left")
          .style("alignment-baseline", "middle")

});