// set the dimensions and margins of the graph
var margin = {top: 50, right: 25, bottom: 50, left: 100},
    width = 775 - margin.left - margin.right,
    height = 350 - margin.top - margin.bottom;

    // var parseTime1 = d3.timeParse("%Y"); 

// set the ranges
var x1 = d3.scaleBand()
  .rangeRound([0, width])
  .paddingInner(0.05)
  .align(0.1);

var y1 = d3.scaleLinear().range([height, 0]);

// define the line
var valuelinef = d3.line()
    .x(function(d) { return x1(d.Year); })
    .y(function(d) { return y1(d.Rate); })
    .curve(d3.curveMonotoneX) ;


// append the ffelsvg obgect to the body of the page
// appends a 'group' element to 'ffelsvg'
// moves the 'group' element to the top left margin
var ffelsvg = d3.select("#FFELrate").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

// Get the data1
d3.csv("FFELrate.csv").then(function(data1) {
// console.log(data1);
  // format the data1
  data1.forEach(function(d) {
      d.Year = d.Year ;
      d.Rate = +d.Rate;
  });

  // Scale the range of the data1
  
  x1.domain(data1.map(function(d) { return d.Year; }));
  y1.domain([0, d3.max(data1, function(d) { return d.Rate; })]);

  // Add the valuelinef path.
  ffelsvg.append("path")
      .data([data1])
      .attr("class", "line")
      .attr("d", valuelinef);   
  
 

  // Add the x1 Axis
  ffelsvg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x1))
      .selectAll("text")
    .style("text-anchor", 'end')
    .attr('dx', '-.8em')
    .attr('dy', '.15em')
    .attr("transform", 'rotate(-55)');

  // Add the y1 Axis
  ffelsvg.append("g")
      .call(d3.axisLeft(y1));

      // Add datapoints
  //     ffelsvg.selectAll(".dot")
  //   .data1(data1)
  // .enter().append("circle") // Uses the enter().append() method
  //   .attr("class", "dot") // Assign a class for styling
  //   .attr("cx", function(d) { return x1(d.Year) })
  //   .attr("cy", function(d) { return y1(d.Rate) })
  //   .attr("r", 4);
    

      // var keys1 = ["Default"]

      // // Usually you have a color1 scale in your chart already
      // var color1 = d3.scaleOrdinal()
      //   .domain(keys1)
      //   .range(["steelblue"]);
      
      // // Add one dot in the legend for each name.
      // var size1 = 20
      // ffelsvg.selectAll("mydot")
      //   .data(keys1)
      //   .enter()
      //   .append("rect")
      //     .attr("x", 120)
      //     .attr("y", function(d,i){ return 50 + i*(size1+5)}) // 100 is where the first dot appears. 25 is the distance between dots
      //     .attr("width", size1)
      //     .attr("height", size1)
      //     .style("fill", function(d){ return color1(d)})
      
      // // Add one dot in the legend for each name.
      // ffelsvg.selectAll("mylabel")
      //   .data(keys1)
      //   .enter()
      //   .append("text")
      //     .attr("x", 120 + size1*1.2)
      //     .attr("y", function(d,i){ return 50 + i*(size1+5) + (size1/2)}) // 100 is where the first dot appears. 25 is the distance between dots
      //     .text(function(d){ return d})
      //     .attr("text-anchor", "left")
      //     .style("alignment-baseline", "middle")

});