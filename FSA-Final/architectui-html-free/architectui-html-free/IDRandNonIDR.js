// // set the dimensions and margins of the graph
// var zmargin = {top: 50, right: 50, bottom: 50, left: 100},
//     zwidth = 1150 - zmargin.left - zmargin.right,
//     zheight = 550 - zmargin.top - zmargin.bottom;

// // append the svg object to the body of the page
// var zsvg = d3.select("#IDR")
//   .append("svg")
//     .attr("width", zwidth + zmargin.left + zmargin.right)
//     .attr("height", zheight + zmargin.top + zmargin.bottom)
//   .append("g")
//     .attr("transform",
//           "translate(" + zmargin.left + "," + zmargin.top + ")");

// //Read the data
// d3.csv("IDR and NON-IDR.csv",

//   // When reading the csv, I must format variables:
//   function(d){
//     return { date : d3.timeParse("%Y-%m-%d")(d.Year), value11: d.IDR, value21: d.Non_IDR }
//   },

//   // Now I can use this dataset:
//   function(data) {

//     // Add X axis --> it is a date format
//     var xz = d3.scaleTime()
//       .domain(d3.extent(data, function(d) { return d.date; }))
//       .range([ 0, zwidth ]);
//     xAxis = zsvg.append("g")
//       .attr("transform", "translate(0," + zheight + ")")
//       .call(d3.axisBottom(xz));
      

//     // Add Y axis
//     var yz = d3.scaleLinear()
//         //  .domain([0, d3.max(data, function(d) { return +d.value21; })])
//             .domain([0, d3.max(data, function(d) { return Math.max(+d.value11, +d.value21); })])
//       .range([ zheight, 0 ]);
//     yAxis = zsvg.append("g")
//       .call(d3.axisLeft(yz));

//     // var y = d3.scaleLinear().range([height, 0]);


//     // Define the 1st Line
//     var valueline11 = d3.line()
//         .x(function(d) { return xz(d.date); })
//         .y(function(d) { return yz(d.value11); });

//     // Define the 2nd Line
//     var valueline21 = d3.line()
//         .x(function(d) { return xz(d.date); })
//         .y(function(d) { return yz(d.value21); });


//     // Add a clipPath: everything out of this area won't be drawn.
//     var clip = zsvg.append("defs").append("svg:clipPath")
//         .attr("id", "clip")
//         .append("svg:rect")
//         .attr("width", zwidth )
//         .attr("height", zheight )
//         .attr("x", 0)
//         .attr("y", 0);

//     // Add brushing
//     var brush = d3.brushX()                   // Add the brush feature using the d3.brush function
//         .extent( [ [0,0], [zwidth,zheight] ] )  // initialise the brush area: start at 0,0 and finishes at width,height: it means I select the whole graph area
//         .on("end", updateChart)               // Each time the brush selection changes, trigger the 'updateChart' function


//     // Add brushing
//     var brush2 = d3.brushX()                   // Add the brush feature using the d3.brush function
//     .extent( [ [0,0], [zwidth,zheight] ] )  // initialise the brush area: start at 0,0 and finishes at width,height: it means I select the whole graph area
//     .on("end", updateChart2)               // Each time the brush selection changes, trigger the 'updateChart' function

//     // Create the line variable: where both the line and the brush take place
//     var line = zsvg.append('g')
//       .attr("clip-path", "url(#clip)")

//     // Add the line1
//     line.append("path")
//       .datum(data)
//       .attr("class", "line")  // I add the class line to be able to modify this line later on.
//       .attr("fill", "none")
//       .attr("stroke", "steelblue")
//       .attr("stroke-width", 1.5)
//       .attr("d", valueline11)

//     // Add the line2
//     line.append("path")
//       .datum(data)
//       .attr("class", "line2")  // I add the class line to be able to modify this line later on.
//       .attr("fill", "none")
//       .attr("stroke", "red")
//       .attr("stroke-width", 1.5)
//       .attr("d", valueline21)

//     // Add the brushing
//     line
//       .append("g")
//         .attr("class", "brush")
//         .call(brush);

//     line
//         .append("g")
//           .attr("class", "brush2")
//           .call(brush2);

//     // A function that set idleTimeOut to null
//     var idleTimeout
//     function idled() { idleTimeout = null; }

//     // A function that update the chart for given boundaries
//     function updateChart() {

//       // What are the selected boundaries?
//       extent = d3.event.selection

//       // If no selection, back to initial coordinate. Otherwise, update X axis domain
//       if(!extent){
//         if (!idleTimeout) return idleTimeout = setTimeout(idled, 350); // This allows to wait a little bit
//         x.domain([ 4,8])
//       }else{
//         x.domain([ x.invert(extent[0]), xz.invert(extent[1]) ])
//         line.select(".brush").call(brush.move, null) // This remove the grey brush area as soon as the selection has been done
//       }

//       // Update axis and line position
//       xAxis.transition().duration(1000).call(d3.axisBottom(xz))
//       line
//           .select('.line')
//           .transition()
//           .duration(1000)
//           .attr("d", valueline11)
//     //   line
//     //       .select('.line')
//     //       .transition()
//     //       .duration(1000)
//     //       .attr("d", valueline21)
//     }

//     function updateChart2() {

//         // What are the selected boundaries?
//         extent = d3.event.selection
  
//         // If no selection, back to initial coordinate. Otherwise, update X axis domain
//         if(!extent){
//           if (!idleTimeout) return idleTimeout = setTimeout(idled, 350); // This allows to wait a little bit
//           x.domain([ 4,8])
//         }else{
//           x.domain([ x.invert(extent[0]), xz.invert(extent[1]) ])
//           line.select(".brush2").call(brush2.move, null) // This remove the grey brush area as soon as the selection has been done
//         }
  
//         // Update axis and line position
//         xAxis.transition().duration(1000).call(d3.axisBottom(xz))
//         line
//             .select('.line2')
//             .transition()
//             .duration(1000)
//             .attr("d", valueline21)
//       }

//     // If user double click, reinitialize the chart
//     zsvg.on("dblclick",function(){
//       x.domain(d3.extent(data, function(d) { return d.date; }))
//       xAxis.transition().call(d3.axisBottom(xz))
//       line
//         .select('.line')
//         .transition()
//         .attr("d", valueline11)
//     //   line
//     //     .select('.line')
//     //     .transition()
//     //     .attr("d", valueline21)
//     });
//     zsvg.on("click",function(){
//         x.domain(d3.extent(data, function(d) { return d.date; }))
//         xAxis.transition().call(d3.axisBottom(xz))
//         line
//           .select('.line2')
//           .transition()
//           .attr("d", valueline21)
//       });

// });